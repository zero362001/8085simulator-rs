def get_opcode(text: str):
	opcode = text.split(' ')[-2]
	try:
		return int(opcode, 16)
	except ValueError:
		print("Error while extracting opcode!")
		return -1

def sort(lines: list):
	dict = {}
	sorted = []
	for line in lines:
		if get_opcode(line) == -1:
			continue
		else:
			dict[get_opcode(line)] = line
	for i in range(255):
		try:
			entry = dict[i]
			line = '\t'.join(entry.split(' ')[1:])
			print(f'{i}\t: {line}')
		except:
			continue

with open('opcodes.txt', 'r') as file:
	lines = file.read().split('\n')
	sort(lines)