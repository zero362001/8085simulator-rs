# 8085simulator-rs

An simple Intel 8085 simulator written in Rust in an effort to learn about systems programming 
while also familiarising myself with the Rust toolset. So far, its just a command line utility but there are
plans of adding GUI interactivity later on.

### About the Intel 8085 Microprocessor:
-----------------------------------------
The 8085 is an 8bit microprocessor with a 16 bit address bus allowing upto 64Kb's of addressable memory!
The general architecture of the 8085 is quite simple and easy to understand for students and new programmers.
There are 6 general purpose registers:
B, C, D, E, H, L along with the *Accumulator* (A). The general purpose registers can also be used as 16bit pairs:
BC, DE, HL denoted by **B**, **D**, and **H** respectively. It features a 16bit *Stack Pointer*, and a 16bit *Program Counter*.
The *Status Register* is an 8bit register representing 5 flags in the following layout:

|   **Bit**   | 128 |  64  |  32 |  16  |  8  |  4  |  2  |  1  |
|---------|-----|------|-----|------|-----|-----|-----|-----|
| **Flag**    |  S  |  Z   |  -  |  AC  |  -  |  P  |  -  |  C  |

S : The Sign bit is turned on if the result obtained in the last operation had its MSB set to 1

Z : The Zero flag is turned on if the result obtained in the last operation was 0

AC : The Auxilliary Carry is turned on if there was a carry in the first nibble of the result in the last operation

P : The parity bit is turned if there were an even number of raised bits in the result of the last operation

C : The carry flag is set if there was a carry in the MSB while computing the last operation

Learn more in the [Wikipedia article](https://en.wikipedia.org/wiki/Intel_8085)

Feel free to contribute in whatever way you desire :)
