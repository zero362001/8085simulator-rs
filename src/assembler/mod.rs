mod lexer;

use lexer::{Lexer, Token, TokenType};
use std::collections::HashMap;

struct Instruction {
    opcode: u8,
    instruction_size: u8,
    argc: u8,
}

impl Instruction {
    pub fn new(opcode: u8, instruction_size: u8, argc: u8) -> Instruction {
        Instruction {
            opcode,
            instruction_size,
            argc,
        }
    }
}

pub struct Assembler<'a> {
    lookup_table: HashMap<&'a str, Instruction>,
    symbol_table: HashMap<&'a str, u16>,
    lexer: Lexer<'a>,
}

impl<'a> Assembler<'a> {
    pub fn new(code: &'a str) -> Assembler {
        let lookup_table: HashMap<&str, Instruction> = HashMap::from([
            ("ADD", Instruction::new(0x80, 1, 1)),
            ("ACI", Instruction::new(0xCE, 2, 1)),
            ("ADC", Instruction::new(0x88, 1, 1)),
            ("ADI", Instruction::new(0xC6, 2, 1)),
            ("ANA", Instruction::new(0xA0, 1, 1)),
            ("ANI", Instruction::new(0xE6, 2, 1)),
            ("CALL", Instruction::new(0xCD, 3, 1)),
            ("CC", Instruction::new(0xDC, 3, 1)),
            ("CM", Instruction::new(0xFC, 3, 1)),
            ("CMA", Instruction::new(0x2F, 1, 0)),
            ("CMC", Instruction::new(0x3D, 1, 0)),
            ("CMP", Instruction::new(0xB8, 1, 1)),
            ("CNC", Instruction::new(0xD4, 3, 1)),
            ("CNZ", Instruction::new(0xC4, 3, 1)),
            ("CP", Instruction::new(0xF4, 3, 1)),
            ("CPE", Instruction::new(0xEC, 3, 1)),
            ("CPI", Instruction::new(0xFE, 2, 1)),
            ("CPO", Instruction::new(0xE4, 3, 1)),
            ("CZ", Instruction::new(0xCC, 3, 1)),
            ("DAA", Instruction::new(0x27, 1, 0)),
            ("DAD", Instruction::new(0x09, 1, 1)),
            ("DCR", Instruction::new(0x05, 1, 1)),
            ("DCX", Instruction::new(0x0B, 1, 1)),
            ("DI", Instruction::new(0xF3, 1, 0)),
            ("EI", Instruction::new(0xFB, 1, 0)),
            ("HLT", Instruction::new(0x76, 1, 0)),
            ("IN", Instruction::new(0xDB, 2, 1)),
            ("INR", Instruction::new(0x04, 1, 1)),
            ("INX", Instruction::new(0x03, 1, 1)),
            ("JC", Instruction::new(0xDA, 3, 1)),
            ("JNC", Instruction::new(0xD2, 3, 1)),
            ("JM", Instruction::new(0xFA, 3, 1)),
            ("JMP", Instruction::new(0xC3, 3, 1)),
            ("JNZ", Instruction::new(0xC2, 3, 1)),
            ("JP", Instruction::new(0xF2, 3, 1)),
            ("JPE", Instruction::new(0xEA, 3, 1)),
            ("JPO", Instruction::new(0xE2, 3, 1)),
            ("JZ", Instruction::new(0xCA, 3, 1)),
            ("LDA", Instruction::new(0x3A, 3, 1)),
            ("LDAX", Instruction::new(0xA, 1, 1)),
            ("LHLD", Instruction::new(0x2A, 3, 1)),
            ("LXI", Instruction::new(0x01, 1, 1)),
            ("MOV", Instruction::new(0x40, 1, 2)),
            ("MVI", Instruction::new(0x06, 2, 2)),
            ("NOP", Instruction::new(0x00, 1, 0)),
            ("ORA", Instruction::new(0xB0, 1, 1)),
            ("ORI", Instruction::new(0xF6, 2, 1)),
            ("OUT", Instruction::new(0xD3, 2, 1)),
            ("PCHL", Instruction::new(0xE9, 1, 0)),
            ("POP", Instruction::new(0xC1, 1, 1)),
            ("POP", Instruction::new(0xF1, 1, 1)),
            ("PUSH", Instruction::new(0xC5, 1, 1)),
            ("RAL", Instruction::new(0x17, 1, 0)),
            ("RAR", Instruction::new(0x1F, 1, 0)),
            ("RC", Instruction::new(0xD8, 1, 0)),
            ("RET", Instruction::new(0xC9, 1, 0)),
            ("RIM", Instruction::new(0x20, 1, 0)),
            ("RLC", Instruction::new(0x07, 1, 0)),
            ("RM", Instruction::new(0xF8, 1, 0)),
            ("RNC", Instruction::new(0xD0, 1, 0)),
            ("RNZ", Instruction::new(0xC0, 1, 0)),
            ("RP", Instruction::new(0xF0, 1, 0)),
            ("RPE", Instruction::new(0xE8, 1, 0)),
            ("RPO", Instruction::new(0xE0, 1, 0)),
            ("RRC", Instruction::new(0x0F, 1, 0)),
            ("RST", Instruction::new(0xC7, 1, 1)),
            ("RZ", Instruction::new(0xC8, 1, 0)),
            ("SBB", Instruction::new(0x98, 1, 1)),
            ("SBI", Instruction::new(0xDE, 2, 1)),
            ("SHLD", Instruction::new(0x22, 3, 1)),
            ("SIM", Instruction::new(0x30, 1, 0)),
            ("SPHL", Instruction::new(0xF9, 1, 0)),
            ("STA", Instruction::new(0x32, 3, 1)),
            ("STAX", Instruction::new(0x02, 1, 1)),
            ("STC", Instruction::new(0x37, 1, 0)),
            ("SUB", Instruction::new(0x90, 1, 1)),
            ("SUI", Instruction::new(0xD6, 2, 1)),
            ("XCHG", Instruction::new(0xEB, 1, 0)),
            ("XRA", Instruction::new(0xA8, 1, 1)),
            ("XRI", Instruction::new(0xEE, 2, 1)),
            ("XTHL", Instruction::new(0xE3, 1, 0)),
        ]);
        let keywords = vec![
            "ADD", "ACI", "ADC", "ADI", "ANA", "ANI", "CALL", "CC", "CM", "CMA", "CMC", "CMP",
            "CNC", "CNZ", "CP", "CPE", "CPI", "CPO", "CZ", "DAA", "DAD", "DCR", "DCX", "DI", "EI",
            "HLT", "IN", "INR", "INX", "JC", "JNC", "JM", "JMP", "JNZ", "JP", "JPE", "JPO", "JZ",
            "LDA", "LDAX", "LHLD", "LXI", "MOV", "MVI", "NOP", "ORA", "ORI", "OUT", "PCHL", "POP",
            "POP", "PUSH", "RAL", "RAR", "RC", "RET", "RIM", "RLC", "RM", "RNC", "RNZ", "RP",
            "RPE", "RPO", "RRC", "RST", "RZ", "SBB", "SBI", "SHLD", "SIM", "SPHL", "STA", "STAX",
            "STC", "SUB", "SUI", "XCHG", "XRA", "XRI", "XTHL",
        ];
        let symbol_table = HashMap::from([]);
        let lexer = Lexer::new(code, keywords);
        return Assembler {
            lookup_table,
            symbol_table,
            lexer,
        };
    }

    // fn get_next(iter: &'a mut Iterator<Item = &'a Token>, expected: TokenType) -> &'a Token {
    //     match iter.next() {
    //         Some(token) => {
    //             match token.token_type {
    //                 expected => token,
    //                 _ => panic!("Expected a {}, found a {}", to_string(expected), to_string(token.token_type))
    //             }
    //         }
    //         None => {
    //             panic!("Expected a {}, reached the end", to_string(expected));
    //         }
    //     }
    // }

    // pub fn assemble(&'a mut self) -> Vec<u8> {
    //     let mut line_number: u16 = 0;
    //     let mut byte_stream: Vec<u8> = vec![];
    //     let tokens = self.lexer.lex();
    //     let mut iter = tokens.iter();
    //     loop {
    //         match iter.next() {
    //             Some(token) => {
    //                 match token.token_type { // get the type of the currently scanned token
    //                     // by design of the assembler, it can only be either an instruction mnemonic
    //                     // or a label defifinition, otherwise we assume the source code to be malformed
    //                     // since we advance the iterator to the required position based on the arguments
    //                     // and structure of a line (refer to the grammar defined above)
    //                     TokenType::Instruction => {
    //                         let instruction = &self.lookup_table[token.lexeme]; // get the instruction metadata
    //                         // such as instruction size, number of arguments, etc; from the instruction table
    //                         let mut opcode = instruction.opcode; // get the base opcode of the instruction
    //                         // we shall modify the opcode by appending the correct offsets based on the provided arguments
    //                         match instruction.argc {
    //                             0 => byte_stream.push(opcode); // the most basic type of instruction without any arguments
    //                             1 => {
    //                                 match token.lexeme {
                                        
    //                                 }
    //                             }
    //                         }
    //                         line_number += instruction.instruction_size; // advance the program counter register by the 
    //                         // size of the currently assembled instruction (used for defining labels)
    //                     },
    //                     TokenType::LabelName => {},
    //                     _ => panic!("Expected either an instruction mnemonic or a label name! found {}", token.lexeme)
    //                 }
    //             }
    //             None => break // we've consumed all tokens and parsed successfully
    //         },
    //     }
    // }

    pub fn assemble(&'a mut self) -> Vec<u8> {
        let mut line: u16 = 0;
        let mut u8_stream = vec![];
        let tokens = self.lexer.lex();
        println!("Lexer output: ");
        for token in &tokens {
            println!("{}", token);
        }
        let mut iter = tokens.iter();
        loop {
            match iter.next() {
                Some(token) => {
                    match token.token_type {
                        TokenType::Instruction => {
                            let instruction = &self.lookup_table[token.lexeme];
                            let mut opcode = instruction.opcode;
                            match instruction.argc {
                                0 => {
                                    u8_stream.push(opcode);
                                },
                                1 => {
                                    match token.lexeme {
                                        "SUI" | "SBI" | "ORI" | "CPI" | "ANI" | "ADI" | "ACI" | "IN" | "OUT" => {
                                            u8_stream.push(instruction.opcode);
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::LiteralHex {
                                                        u8_stream.push(parse_hex_u8(arg1.lexeme));
                                                    } else if arg1.token_type == TokenType::LiteralDec {
                                                        u8_stream.push(parse_u8(arg1.lexeme));
                                                    } else {
                                                        panic!("Expected integer literal, found {}", arg1.lexeme);
                                                    }
                                                },
                                                None => panic!("Expected an integer literal, found EOF!")
                                            }
                                        },
                                        "ADD" | "ADC" | "ANA" | "ORA" | "SUB" | "CMP" | "SBB" | "XRA" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::Register {
                                                        opcode += get_register_index(arg1.lexeme);
                                                    } else if arg1.token_type == TokenType::RegisterPair {
                                                        opcode += get_register_pair_index(arg1.lexeme);
                                                    }
                                                    u8_stream.push(opcode);
                                                },
                                                None => panic!("Expected an integer literal, found EOF!")
                                            }
                                        },
                                        "CALL" | "CC" | "CM" | "CNC" | "CNZ" | "CP" | "CPE" | "CPO" | "CZ" | "JC" |
                                        "JM" | "JMP" | "JNC" | "JNZ" | "JP" | "JPE" | "JPO" | "JZ" => {
                                            u8_stream.push(instruction.opcode);
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::LabelName {
                                                        if self.symbol_table.contains_key(arg1.lexeme) {
                                                            let val = self.symbol_table[arg1.lexeme];
                                                            u8_stream.push((val << 8 >> 8) as u8);
                                                            u8_stream.push((val >> 8) as u8);
                                                        } else {
                                                            panic!("Label: {} is not defined!", token.lexeme);
                                                        }
                                                    }
                                                },
                                                None => panic!("Expected label name, found EOF!")
                                            };
                                        },
                                        "LDA" | "LHLD" | "SHLD" | "STA" => {
                                            u8_stream.push(instruction.opcode);
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::LiteralHex {
                                                        let val = parse_hex_word(arg1.lexeme);
                                                        u8_stream.push((val << 8 >> 8) as u8);
                                                        u8_stream.push((val >> 8) as u8);
                                                    } else if arg1.token_type == TokenType::LiteralDec {
                                                        let val = parse_word(arg1.lexeme);
                                                        u8_stream.push((val << 8 >> 8) as u8);
                                                        u8_stream.push((val >> 8) as u8);
                                                    } else {
                                                        panic!("Expected an integer literal, found: {}", arg1.lexeme);
                                                    }
                                                },
                                                None => {
                                                    panic!("Expected an integer literal, found EOF!");
                                                }
                                            }
                                        },
                                        "DAD" | "DCX" | "INX" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::RegisterPair || arg1.token_type == TokenType::Register {
                                                        opcode += get_register_pair_index(arg1.lexeme) * 16;
                                                    } else {
                                                        panic!("Expected a register pair name, found {}", arg1.lexeme)
                                                    }
                                                },
                                                None => panic!("Expected a register pair name, found EOF!")
                                            }
                                            u8_stream.push(opcode);
                                        },
                                        "LDAX" | "STAX" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::RegisterPair || arg1.token_type == TokenType::Register {
                                                        let pair_index = get_register_pair_index(arg1.lexeme) * 16;
                                                        if pair_index > 2 {
                                                            panic!("Valid register pairs: <B, D>")
                                                        } else {
                                                            opcode += pair_index;
                                                        }
                                                    } else {
                                                        panic!("Expected a register pair name, found {}", arg1.lexeme)
                                                    }
                                                },
                                                None => panic!("Expected a register pair name, found EOF!")
                                            }
                                            u8_stream.push(opcode);
                                        },
                                        "PUSH" | "POP" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::RegisterPair || arg1.token_type == TokenType::Register {
                                                        let pair_index = get_register_pair_index(arg1.lexeme) * 16;
                                                        if arg1.lexeme == "SP" {
                                                            panic!("SP cannot be used in this context!")
                                                        } else {
                                                            opcode += pair_index;
                                                        }
                                                    } else {
                                                        panic!("Expected a register pair name, found {}", arg1.lexeme)
                                                    }
                                                },
                                                None => panic!("Expected a register pair name, found EOF!")
                                            }
                                            u8_stream.push(opcode);
                                        },
                                        "RST" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    opcode += match arg1.lexeme {
                                                        "0" => 0,
                                                        "1" => 8,
                                                        "2" => 16,
                                                        "3" => 24,
                                                        "4" => 32,
                                                        "5" => 40,
                                                        "6" => 48,
                                                        "7" => 56,
                                                        _ => panic!("Invalid argument! Allowed numbers: {{ 0-7 }}")
                                                    }
                                                },
                                                None => panic!("Expected an integer, found EOF!")
                                            }
                                            u8_stream.push(opcode);
                                        }
                                        _ => panic!("Invalid instruction! No instruction exists with the mnemonic {}", token.lexeme)
                                    };
                                },
                                2 => {
                                    match token.lexeme {
                                        "MOV" => {
                                            let mut opcode = instruction.opcode;
                                            match iter.next() {
                                                Some(arg1) => {
                                                    if arg1.token_type == TokenType::Register {
                                                        opcode += 8 * get_register_index(arg1.lexeme);
                                                        match iter.next() {
                                                            Some(comma) => {
                                                                if comma.token_type == TokenType::Comma {
                                                                    match iter.next() {
                                                                        Some(arg2) => {
                                                                            if arg2.token_type == TokenType::Register {
                                                                                opcode += get_register_index(arg2.lexeme);
                                                                            } else {
                                                                                panic!("Expected a register name, found {}", arg2.lexeme)
                                                                            }
                                                                        },
                                                                        None => panic!("Expected a register name, found EOF!")
                                                                    }
                                                                } else {
                                                                    panic!("Expected a comma")
                                                                }
                                                            },
                                                            None => panic!("Expected a comma")
                                                        }
                                                    }
                                                },
                                                None => panic!("Expected a register name, found EOF")
                                            }
                                            u8_stream.push(opcode);
                                        },
                                        "MVI" => {
                                            match iter.next() {
                                                Some(arg1) => {
                                                    u8_stream.push(match arg1.lexeme {
                                                        "A" => 0x3e,
                                                        "B" => 0x06,
                                                        "C" => 0x0e,
                                                        "D" => 0x16,
                                                        "E" => 0x1e,
                                                        "H" => 0x26,
                                                        "L" => 0x2e,
                                                        "M" => 0x36,
                                                        _ => panic!("Not a valid register!")
                                                    });
                                                    match iter.next() {
                                                        Some(comma) => {
                                                            if comma.token_type == TokenType::Comma {
                                                                match iter.next() {
                                                                    Some(arg2) => {
                                                                        if arg2.token_type == TokenType::LiteralHex {
                                                                            let val = parse_hex_u8(arg2.lexeme);
                                                                            u8_stream.push(val);
                                                                        } else if arg2.token_type == TokenType::LiteralDec {
                                                                            let val = parse_u8(arg2.lexeme);
                                                                            u8_stream.push(val);
                                                                        } else {
                                                                            panic!("Expected an integer literal, found: {}", arg1.lexeme);
                                                                        }
                                                                    },
                                                                    None => panic!("Expected a register name, found EOF")
                                                                }
                                                            } else {
                                                                panic!("Expected a comma")
                                                            }
                                                        },
                                                        None => panic!("Expected a comma")
                                                    }
                                                },
                                                None => panic!("Expected a register name, found EOF")
                                            }
                                        },
                                        _ => panic!("Invalid instruction!")
                                    }
                                },
                                _ => panic!("Invalid instruction! All instructions take at most 2 arguments!")
                            }
                            line += instruction.instruction_size as u16;
                        }
                        TokenType::LabelName => {
                            match iter.next() {
                                Some(next) => {
                                    if next.token_type == TokenType::Colon {
                                        if self.symbol_table.contains_key(token.lexeme) {
                                            panic!("Label already exists! Cannot redefine label");
                                        }
                                        self.symbol_table.insert(token.lexeme, line);
                                    } else {
                                        panic!(
                                            "Expected a colon after label name, found {}",
                                            next.lexeme
                                        )
                                    }
                                }
                                None => panic!("Expected a colon after label name, found EOF"),
                            };
                        }
                        _ => panic!("Expected either an instruction or a label name"),
                    };
                }
                None => break,
            };
        }
        u8_stream
    }
}

fn get_register_index(name: &str) -> u8 {
    match name {
        "A" => 7,
        "B" => 0,
        "C" => 1,
        "D" => 2,
        "E" => 3,
        "H" => 4,
        "L" => 5,
        "M" => 6,
        _ => panic!("Not a register name!"),
    }
}
fn get_register_pair_index(name: &str) -> u8 {
    match name {
        "B" => 0,
        "D" => 1,
        "H" => 2,
        "SP" | "PSW" => 3,
        _ => panic!("Invalid register pair name!"),
    }
}

fn parse_hex_u8(string: &str) -> u8 {
    match u8::from_str_radix(&string[..string.len() - 1], 16) {
        Ok(parsed) => parsed,
        _ => panic!("Couldn't parse integer"),
    }
}

fn parse_hex_word(string: &str) -> u16 {
    match u16::from_str_radix(&string[..string.len() - 1], 16) {
        Ok(parsed) => parsed,
        _ => panic!("Couldn't parse integer"),
    }
}

fn parse_u8(string: &str) -> u8 {
    match u8::from_str_radix(string, 10) {
        Ok(parsed) => parsed,
        _ => panic!("Couldn't parse integer"),
    }
}

fn parse_word(string: &str) -> u16 {
    match u16::from_str_radix(string, 10) {
        Ok(parsed) => parsed,
        _ => panic!("Couldn't parse integer"),
    }
}

fn parse_literal_u8(token: &Token) -> u8 {
    match token.token_type {
        TokenType::LiteralHex => parse_hex_u8(token.lexeme),
        TokenType::LiteralDec => parse_u8(token.lexeme),
        _ => panic!("Failed to parse literal!"),
    }
}

fn parse_literal_word(token: &Token) -> u16 {
    match token.token_type {
        TokenType::LiteralHex => parse_hex_word(token.lexeme),
        TokenType::LiteralDec => parse_word(token.lexeme),
        _ => panic!("Failed to parse literal!"),
    }
}

// fn to_string(token_type: TokenType) -> String {
//     match token_type {
//         TokenType::LiteralDec => String::from("Decimal Literal"),
//         TokenType::LiteralHex => String::from("Hexadecimal Literal"),
//         TokenType::Instruction => String::from("Instruction mnemonic"),
//         TokenType::LabelName => String::from("Label name"),
//         TokenType::Comma => String::from("Comma"),
//         TokenType::Colon => String::from("Colon"),
//         TokenType::Register => String::from("Register"),
//         TokenType::RegisterPair => String::from("Register pair")
//     }
// }
