#[derive(PartialEq)]
pub enum TokenType {
    LiteralHex,
    LiteralDec,
    LabelName,
    Colon,
    Comma,
    Register,
    RegisterPair,
    Instruction,
    Error,
}

pub struct Token<'a> {
    pub token_type: TokenType,
    pub lexeme: &'a str,
    pub value: u8,
}

impl<'a> Token<'a> {
    pub fn new(lexeme: &'a str, token_type: TokenType, value: u8) -> Token<'a> {
        return Token { token_type, lexeme, value, };
    }
}

impl std::fmt::Display for Token<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "lexeme: {}; type: {}",
            self.lexeme,
            match self.token_type {
                TokenType::LiteralDec => "Decimal",
                TokenType::LiteralHex => "Hexadecimal",
                TokenType::LabelName => "Label",
                TokenType::Instruction => "Instruction",
                TokenType::Comma => "Comma",
                TokenType::Colon => "Colon",
                TokenType::Register => "Register",
                TokenType::RegisterPair => "RegisterPair",
                _ => "Error",
            }
        )
    }
}

pub struct Lexer<'a> {
    code: &'a str,
    keywords_table: Vec<u64>,
}

impl<'a> Lexer<'a> {
    pub fn new(code: &'a str, keywords: Vec<&str>) -> Lexer<'a> {
        let mut keywords_table: Vec<u64> = vec![];
        for string in keywords {
            let hash = djb2(string);
            keywords_table.push(hash);
        }
        return Lexer {
            code, keywords_table
        }
    }

    fn is_keyword(&self, code: &str) -> bool {
        let hash = djb2(code);
        self.keywords_table.contains(&hash)
    }

    pub fn lex(&mut self) -> Vec<Token> {
        let mut token_stream = Vec::<Token>::new();
        let bytes = self.code.as_bytes();
        let mut token_start: usize = 0;
        let mut token_end: usize = 0;
        for (i, &item) in bytes.iter().enumerate() {
            if item == b',' || item == b':' {
                if token_start != token_end {
                    let token = &self.code[token_start..i];
                    let literal_result = is_literal(token);
                    if literal_result == 1 {
                        let actual_token = Token::new(token, TokenType::LiteralDec, 0);
                        token_stream.push(actual_token);
                    } else if literal_result == 2 {
                        let actual_token = Token::new(token, TokenType::LiteralHex, 0);
                        token_stream.push(actual_token);
                    } else if self.is_keyword(token) {
                        let actual_token = Token::new(token, TokenType::Instruction, 0);
                        token_stream.push(actual_token);
                    } else if is_register(token) {
                        let actual_token = Token::new(token, TokenType::Register, 0);
                        token_stream.push(actual_token);
                    } else if is_valid_name(token) {
                        let actual_token = Token::new(token, TokenType::LabelName, 0);
                        token_stream.push(actual_token);
                    }
                }
                let actual_token = Token::new(
                    &self.code[i..i + 1],
                    match item {
                        b':' => TokenType::Colon,
                        b',' => TokenType::Comma,
                        _ => TokenType::Error,
                    },
                    0,
                );
                token_stream.push(actual_token);
                token_start = i + 1;
                token_end = i + 1;
            } else if item == b' ' || item == b'\t' || item == b'\n' {
                if token_start != token_end {
                    let token = &self.code[token_start..i];
                    let literal_result = is_literal(token);
                    if literal_result == 1 {
                        let actual_token = Token::new(token, TokenType::LiteralDec, 0);
                        token_stream.push(actual_token);
                    } else if literal_result == 2 {
                        let actual_token = Token::new(token, TokenType::LiteralHex, 0);
                        token_stream.push(actual_token);
                    } else if self.is_keyword(token) {
                        let actual_token = Token::new(token, TokenType::Instruction, 0);
                        token_stream.push(actual_token);
                    } else if is_register(token) {
                        let actual_token = Token::new(token, TokenType::Register, 0);
                        token_stream.push(actual_token);
                    } else if is_valid_name(token) {
                        let actual_token = Token::new(token, TokenType::LabelName, 0);
                        token_stream.push(actual_token);
                    }
                }
                token_start = i + 1;
                token_end = i + 1;
            } else {
                token_end += 1;
            }
        }
        if token_start != token_end {
            let token = &self.code[token_start..token_end];
            let literal_result = is_literal(token);
            if literal_result == 1 {
                let actual_token = Token::new(token, TokenType::LiteralDec, 0);
                token_stream.push(actual_token);
            } else if literal_result == 2 {
                let actual_token = Token::new(token, TokenType::LiteralHex, 0);
                token_stream.push(actual_token);
            } else if self.is_keyword(token) {
                let actual_token = Token::new(token, TokenType::Instruction, 0);
                token_stream.push(actual_token);
            } else if is_register(token) {
                let actual_token = Token::new(token, TokenType::Register, 0);
                token_stream.push(actual_token);
            } else if is_valid_name(token) {
                let actual_token = Token::new(token, TokenType::LabelName, 0);
                token_stream.push(actual_token);
            }
        }
        token_stream
    }
}

pub fn djb2(string: &str) -> u64 {
    let mut hash: u64 = 5381;
    let bytes = string.as_bytes();
    for byte in bytes {
        hash = ((hash << 5) + hash) + (*byte as u64);
    }
    return hash;
}

fn is_register(string: &str) -> bool {
    match string {
        "A" | "B" | "C" | "D" | "H" | "L" | "M" | "SP" | "PSW" => true,
        _ => false,
    }
}

fn is_valid_name(string: &str) -> bool {
    let bytes = string.as_bytes();
    let mut flag = false;
    for (_i, &item) in bytes.iter().enumerate() {
        if (item >= b'a' && item <= b'z') || (item >= b'A' && item <= b'Z') || item == b'_' {
            flag = true;
        } else if !(item >= b'0' && item <= b'9' && flag) {
            print!("{}", item);
            return false;
        }
    }
    true
}

fn is_literal(string: &str) -> u8 {
    let bytes = string.as_bytes();
    let mut flag1 = false;
    let mut flag2 = false;
    for (_i, &item) in bytes.iter().enumerate() {
        if item >= b'0' && item <= b'9' {
            if flag2 {
                return 0;
            } else {
                flag1 = true;
            }
        } else if item == b'H' || item == b'h' {
            if !flag1 || flag2 {
                return 0;
            } else {
                flag2 = true;
            }
        } else {
            return 0;
        }
    }
    match flag2 { true => 2, false => 1 }
}