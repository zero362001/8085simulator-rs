use gtk::prelude::*;
use gtk::{ Application, ApplicationWindow, Button };

const APP_ID: &str = "tech.zifmann.8085sim_rs";

pub fn create_gtk_app() -> Application {
    Application::builder().application_id(APP_ID).build()
}

pub fn setup_app(app: &mut Application) {
    app.connect_activate(build_ui);
    app.run();
}

pub fn build_ui(app: &Application) {
    let button = Button::builder()
        .label("press me!")
        .margin_top(10)
        .margin_bottom(10)
        .margin_start(10)
        .margin_end(10)
        .build();
    button.connect_clicked(move |button| {
        button.set_label("Hello, world!");
    });
    let window = ApplicationWindow::builder()
        .application(app)
        .title("8085 Simulator")
        .child(&button)
        .build();
    window.present();
}