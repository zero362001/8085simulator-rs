pub type Byte = u8;
pub type Word = u16;
pub type Instruction = fn(&mut Microcontroller);

use crate::operations;
use std::num;

pub enum Register {
    A, B, C, D, E, H, L, M, F, SP, PSW
}

pub struct RegisterArray {
    pub register_a      : Byte,
    pub register_b      : Byte,
    pub register_c      : Byte,
    pub register_d      : Byte,
    pub register_e      : Byte,
    pub register_h      : Byte,
    pub register_l      : Byte,
    pub stack_pointer   : Word,
    pub program_counter : Word,
    pub status_register : Byte
    // Status Register layout:
    // | 128  | 64   | 32  | 16 | 8 | 4 | 2 | 1 |
    // | Sign | Zero |  -  | AC | - | P | - | C |
}

impl RegisterArray {
    pub fn new() -> RegisterArray {
        let accm : Byte = 0;
        let regb : Byte = 0;
        let regc : Byte = 0;
        let regd : Byte = 0;
        let rege : Byte = 0;
        let regh : Byte = 0;
        let regl : Byte = 0;
        let stpt : Word = 0;
        let prct : Word = 0;
        let stat : Byte = 0;

        return RegisterArray {
            register_a      : accm,
            register_b      : regb,
            register_c      : regc,
            register_d      : regd,
            register_e      : rege,
            register_h      : regh,
            register_l      : regl,
            stack_pointer   : stpt,
            program_counter : prct,
            status_register : stat
        }
    }
}

pub struct Microcontroller {
    pub register_array  : RegisterArray,
    pub memory          : [Byte; 65536],
    pub io_ports        : [Byte; 255],
    pub op_table        : [Instruction; 256],
    running             : bool,
    interrupts          : bool
}

impl Microcontroller {
    
    pub fn new() -> Microcontroller {
        let register_array  : RegisterArray     = RegisterArray::new();
        let memory          : [Byte; 65536]       = [0; 65536];
        let io_ports        : [Byte; 255]         = [0; 255];
        let running         : bool              = false;
        let interrupts      : bool              = false;

        let op_table        : [Instruction; 256] = [
            operations::noop, // 0
            operations::lxi_b, // 1
            operations::stax_b, // 2
            operations::inx_b, // 3
            operations::inr_b, // 4
            operations::dcr_b, // 5
            operations::mvi_b, // 6
            operations::rlc, // 7
            operations::noop, // 8
            operations::dad_b, // 9
            operations::ldax_b, // a
            operations::dcx_b, // b
            operations::inr_c, // c
            operations::dcr_c, // d
            operations::mvi_c, // e
            operations::rrc, // f
            operations::noop, // 10
            operations::lxi_d, // 11
            operations::stax_d, // 12
            operations::inx_d, // 13
            operations::inr_d, // 14
            operations::dcr_d, // 15
            operations::mvi_d, // 16
            operations::ral, // 17
            operations::noop, // 18
            operations::dad_d, // 19
            operations::ldax_d, // 1a
            operations::dcx_d, // 1b
            operations::inr_e, // 1c
            operations::dcr_e, // 1d
            operations::mvi_e, // 1e
            operations::rar, // 1f
            operations::rim, // 20
            operations::lxi_h, // 21
            operations::shld, // 22
            operations::inx_h, // 23
            operations::inr_h, // 24
            operations::dcr_h, // 25
            operations::mvi_h, // 26
            operations::daa, // 27
            operations::noop, // 28
            operations::dad_d, // 29
            operations::lhld, // 2a
            operations::dcx_h, // 2b
            operations::inr_l, // 2c
            operations::dcr_l, // 2d
            operations::mvi_l, // 2e
            operations::cma, // 2f
            operations::sim, // 30
            operations::lxi_sp, // 31
            operations::sta, // 32
            operations::inx_sp, // 33
            operations::inr_m, // 34
            operations::dcr_m, // 35
            operations::mvi_m, // 36
            operations::stc, // 37
            operations::noop, // 38
            operations::dad_sp, // 39
            operations::lda, // 3a
            operations::dcx_sp, // 3b
            operations::inr_a, // 3c
            operations::dcr_a, // 3d
            operations::mvi_a, // 3e
            operations::cmc, // 3f
            operations::mov_b_b, // 40
            operations::mov_b_c, // 41
            operations::mov_b_d, // 42
            operations::mov_b_e, // 43
            operations::mov_b_h, // 44
            operations::mov_b_l, // 45
            operations::mov_b_m, // 46
            operations::mov_b_a, // 47
            operations::mov_c_b, // 48
            operations::mov_c_c, // 49
            operations::mov_c_d, // 4a
            operations::mov_c_e, // 4b
            operations::mov_c_h, // 4c
            operations::mov_c_l, // 4d
            operations::mov_c_m, // 4e
            operations::mov_c_a, // 4f
            operations::mov_d_b, // 50
            operations::mov_d_c, // 51
            operations::mov_d_d, // 52
            operations::mov_d_e, // 53
            operations::mov_d_h, // 54
            operations::mov_d_l, // 55
            operations::mov_d_m, // 56
            operations::mov_d_a, // 57
            operations::mov_e_b, // 58
            operations::mov_e_c, // 59
            operations::mov_e_d, // 5a
            operations::mov_e_e, // 5b
            operations::mov_e_h, // 5c
            operations::mov_e_l, // 5d
            operations::mov_e_m, // 5e
            operations::mov_e_a, // 5f
            operations::mov_h_b, // 60
            operations::mov_h_c, // 61
            operations::mov_h_d, // 62
            operations::mov_h_e, // 63
            operations::mov_h_h, // 64
            operations::mov_h_l, // 65
            operations::mov_h_m, // 66
            operations::mov_h_a, // 67
            operations::mov_l_b, // 68
            operations::mov_l_c, // 69
            operations::mov_l_d, // 6a
            operations::mov_l_e, // 6b
            operations::mov_l_h, // 6c
            operations::mov_l_l, // 6d
            operations::mov_l_m, // 6e
            operations::mov_l_a, // 6f
            operations::mov_m_b, // 70
            operations::mov_m_c, // 71
            operations::mov_m_d, // 72
            operations::mov_m_e, // 73
            operations::mov_m_h, // 74
            operations::mov_m_l, // 75
            operations::hlt, // 76
            operations::mov_m_a, // 77
            operations::mov_a_b, // 78
            operations::mov_a_c, // 79
            operations::mov_a_d, // 7a
            operations::mov_a_e, // 7b
            operations::mov_a_h, // 7c
            operations::mov_a_l, // 7d
            operations::mov_a_m, // 7e
            operations::mov_a_a, // 7f
            operations::add_b, // 80
            operations::add_c, // 81
            operations::add_d, // 82
            operations::add_e, // 83
            operations::add_h, // 84
            operations::add_l, // 85
            operations::add_m, // 86
            operations::add_a, // 87
            operations::adc_b, // 88
            operations::adc_c, // 89
            operations::adc_d, // 8a
            operations::adc_e, // 8b
            operations::adc_h, // 8c
            operations::adc_l, // 8d
            operations::adc_m, // 8e
            operations::adc_a, // 8f
            operations::sub_b, // 90
            operations::sub_c, // 91
            operations::sub_d, // 92
            operations::sub_e, // 93
            operations::sub_h, // 94
            operations::sub_l, // 95
            operations::sub_m, // 96
            operations::sub_a, // 97
            operations::sbb_b, // 98
            operations::sbb_c, // 99
            operations::sbb_d, // 9a
            operations::sbb_e, // 9b
            operations::sbb_h, // 9c
            operations::sbb_l, // 9d
            operations::sbb_m, // 9e
            operations::sbb_a, // 9f
            operations::ana_b, // a0
            operations::ana_c, // a1
            operations::ana_d, // a2
            operations::ana_e, // a3
            operations::ana_h, // a4
            operations::ana_l, // a5
            operations::ana_m, // a6
            operations::ana_a, // a7
            operations::xra_b, // a8
            operations::xra_c, // a9
            operations::xra_d, // aa
            operations::xra_e, // ab
            operations::xra_h, // ac
            operations::xra_l, // ad
            operations::xra_m, // ae
            operations::xra_a, // af
            operations::ora_b, // b0
            operations::ora_c, // b1
            operations::ora_d, // b2
            operations::ora_e, // b3
            operations::ora_h, // b4
            operations::ora_l, // b5
            operations::ora_m, // b6
            operations::ora_a, // b7
            operations::cmp_b, // b8
            operations::cmp_c, // b9
            operations::cmp_d, // ba
            operations::cmp_e, // bb
            operations::cmp_h, // bc
            operations::cmp_l, // bd
            operations::cmp_m, // be
            operations::cmp_a, // bf
            operations::rnz, // c0
            operations::pop_b, // c1
            operations::jnz, // c2
            operations::jmp, // c3
            operations::cnz, // c4
            operations::push_b, // c5
            operations::adi, // c6
            operations::rst_0, // c7
            operations::rz, // c8
            operations::ret, // c9
            operations::jz, // ca
            operations::noop, // cb
            operations::cz, // cc
            operations::call, // cd
            operations::aci, // ce
            operations::rst_1, // cf
            operations::rnc, // d0
            operations::pop_d, // d1
            operations::jnc, // d2
            operations::output, // d3
            operations::cnc, // d4
            operations::push_d, // d5
            operations::sui, // d6
            operations::rst_2, // d7
            operations::rc, // d8
            operations::noop, // d9
            operations::jc, // da
            operations::input, // db
            operations::cc, // dc
            operations::noop, // dd
            operations::sbi, // de
            operations::rst_3, // df
            operations::rpo, // e0
            operations::pop_h, // e1
            operations::jpo, // e2
            operations::xthl, // e3
            operations::cpo, // e4
            operations::push_h, // e5
            operations::ani, // e6
            operations::rst_4, // e7
            operations::rpe, // e8
            operations::pchl, // e9
            operations::jpe, // ea
            operations::xchg, // eb
            operations::cpe, // ec
            operations::noop, // ed
            operations::xri, // ee
            operations::rst_5, // ef
            operations::rp, // f0
            operations::pop_psw, // f1
            operations::jp, // f2
            operations::di, // f3
            operations::cp, // f4
            operations::push_psw, // f5
            operations::ori, // f6
            operations::rst_6, // f7
            operations::rm, // f8
            operations::sphl, // f9
            operations::jm, // fa
            operations::ei, // fb
            operations::cm, // fc
            operations::noop, // fd
            operations::cpi, // fe
            operations::noop  // ff
        ];

        return Microcontroller {
            register_array  : register_array,
            memory          : memory,
            io_ports        : io_ports,
            op_table        : op_table,
            running         : running,
            interrupts      : interrupts
        }
    }

    pub fn disable_interrupts(&mut self) {
        self.interrupts = false;
    }

    pub fn enable_interrupts(&mut self) {
        self.interrupts = true;
    }

    pub fn fetch(&mut self) -> Byte {
        let ir = self.memory[self.register_array.program_counter as usize];
        if self.register_array.program_counter >= 65535 {
            panic!("Reached end of memory looking for newer instructions to execute! This shouldn't happen under normal circumstances. Are you missing a 'HLT' instruction at the end?");
        }
        self.register_array.program_counter += 1;
        return ir;
    }

    pub fn fetch_word(&mut self) -> Word {
        let mut word = self.fetch() as Word;
        word |= (self.fetch() as Word) << 8;
        return word;
    }

    pub fn read_io(&mut self, port : Byte) -> Byte {
        self.io_ports[port as usize]
    }

    pub fn write_io(&mut self, port : Byte, data : Byte) {
        self.io_ports[port as usize] = data;
    }
    
    pub fn execute(&mut self) {
        let op = self.fetch();
        self.op_table[op as usize](self);
    }

    pub fn tick(&mut self) {
        while self.running {
            self.execute();
        }
    }

    pub fn start(&mut self) {
        self.running = true;
    }

    pub fn stop(&mut self) {
        self.running = false;
    }

    pub fn get_register(&self, name : Register) -> Byte {
        match name {
            Register::A => self.register_array.register_a,
            Register::B => self.register_array.register_b,
            Register::C => self.register_array.register_c,
            Register::D => self.register_array.register_d,
            Register::E => self.register_array.register_e,
            Register::H => self.register_array.register_h,
            Register::L => self.register_array.register_l,
            Register::F => self.register_array.status_register,
            Register::M => self.memory[self.get_register_pair(Register::H) as usize],
            _ => panic!("Operation not permitted!")
        }
    }

    pub fn set_register(&mut self, name : Register, value : Byte) {
        match name {
            Register::A => self.register_array.register_a = value,
            Register::B => self.register_array.register_b = value,
            Register::C => self.register_array.register_c = value,
            Register::D => self.register_array.register_d = value,
            Register::E => self.register_array.register_e = value,
            Register::H => self.register_array.register_h = value,
            Register::L => self.register_array.register_l = value,
            Register::F => self.register_array.status_register = value,
            Register::M => self.memory[self.get_register_pair(Register::H) as usize] = value,
            _ => panic!("Operation not permitted!")
        };
    }

    pub fn get_stack_pointer(&self) -> Word {
        self.register_array.stack_pointer
    }

    pub fn get_program_counter(&self) -> Word {
        self.register_array.program_counter
    }

    pub fn set_stack_pointer(&mut self, value : Word) {
        self.register_array.stack_pointer = value;
    }

    pub fn set_program_counter(&mut self, value : Word) {
        self.register_array.program_counter = value;
    }

    pub fn get_register_pair(&self, name : Register) -> Word {
        match name {
            Register::B => ((self.register_array.register_b as Word) << 8) + (self.register_array.register_c as Word),
            Register::D => ((self.register_array.register_d as Word) << 8) + (self.register_array.register_e as Word),
            Register::H => ((self.register_array.register_h as Word) << 8) + (self.register_array.register_l as Word),
            Register::SP => ((self.register_array.stack_pointer)),
            Register::PSW => ((self.register_array.status_register as Word) << 8) + (self.register_array.register_a as Word),
            _   => 0
        }
    }

    pub fn set_register_pair(&mut self, name : Register, value : Word) {
        match name {
            Register::B =>  {
                        self.register_array.register_b = (value >> 8) as Byte;
                        self.register_array.register_c = (value & 0b0000000011111111) as Byte;
                    },
            Register::D =>  {
                        self.register_array.register_d = (value >> 8) as Byte;
                        self.register_array.register_e = (value & 0b0000000011111111) as Byte;
                    },
            Register::H =>  {
                        self.register_array.register_h = (value >> 8) as Byte;
                        self.register_array.register_l = (value & 0b0000000011111111) as Byte;
                    },
            Register::PSW => {
                        self.register_array.register_a = (value >> 8) as Byte;
                        self.register_array.status_register = (value & 0b0000000011111111) as Byte;
                    },
            Register::SP => self.register_array.stack_pointer = value,
            _   => return
        };
    }

    pub fn get_status(&self) -> Byte {
        self.register_array.status_register
    }

    pub fn set_status(&mut self, status : Byte) {
        self.register_array.status_register = status;
    }

    pub fn check_flag(&self, flag : char) -> bool {
        match flag {
            'a' => self.register_array.status_register & 0b00010000 == 0b00010000,
            's' => self.register_array.status_register & 0b10000000 == 0b10000000,
            'z' => self.register_array.status_register & 0b01000000 == 0b01000000,
            'c' => self.register_array.status_register & 0b00000001 == 0b00000001,
            'p' => self.register_array.status_register & 0b00000100 == 0b00000100,
            _   => false
        }
    }

    pub fn set_flag(&mut self, flag : char, val : bool) {
        let status = &mut self.register_array.status_register;
        match flag {
            'a' => *status = *status & 0b11101111 | (0b00010000 * val as Byte),
            'c' => *status = *status & 0b11111110 | (0b00000001 * val as Byte),
            's' => *status = *status & 0b01111111 | (0b10000000 * val as Byte),
            'z' => *status = *status & 0b10111111 | (0b01000000 * val as Byte),
            'p' => *status = *status & 0b11111011 | (0b00000100 * val as Byte),
            _   => println!("No such flag available!")
        };
    }

    pub fn update_flags(&mut self) {
        let val = self.get_register(Register::A) as i32;
        if val >= 128 {
            self.set_flag('s', true);
        }
        let mut count = 0;
        let two : i32 = 2;
        for x in 0..7 {
            if val & two.pow(x) == two.pow(x) {
                count += 1;
            }
        }
        if count % 2 == 0 {
            self.set_flag('p', true);
        }
    }

    pub fn is_running(&self) -> bool {
        self.running
    }

    pub fn print(&self) {
        println!("\nMicrocontroller State:");
        println!(" _______________________________");
        println!("|                               |");
        println!("|           REGISTERS           |");
        println!("|_______________________________|");
        println!("| A\t{}\t| F\t{}\t|", self.register_array.register_a, self.register_array.status_register);
        println!("| C\t{}\t| B\t{}\t|", self.register_array.register_b, self.register_array.register_c);
        println!("| A\t{}\t| B\t{}\t|", self.register_array.register_d, self.register_array.register_e);
        println!("| H\t{}\t| L\t{}\t|", self.register_array.register_h, self.register_array.register_l);
        println!("|_______________________________|");
        println!("| SPECIAL REGISTERS             |");
        println!("|_______________________________|");
        println!("| PC\t{}\t| STP\t{}\t|", self.register_array.program_counter, self.register_array.stack_pointer);
        println!("| FLAGS:                        |");
        println!("|  S  |  Z  |  AC  |  P  |  C   |");
        println!("|  {}  |  {}  |  {}   |  {}  |  {}   |", 
            self.check_flag('s') as Byte, 
            self.check_flag('z') as Byte, 
            self.check_flag('a') as Byte,
            self.check_flag('p') as Byte,
            self.check_flag('c') as Byte
        );
        println!("|_______________________________|");
    }

}
