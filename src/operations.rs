//mod microcontroller;
use crate::microcontroller::Microcontroller;
use crate::microcontroller::Byte;
use crate::microcontroller::Word;
use crate::microcontroller::Register;
use std::cmp::Ordering;


fn add(controller : &mut Microcontroller, b : Byte, c : Byte) {
    let a = controller.get_register(Register::A);
    let sum : Byte = a + b + c;
    let ac = (((a & 0b00001111) + c) + (b & 0b00001111)) > 0b00001001;
    let c = (a as Word + b as Word + c as Word) > 255;
    controller.set_flag('z', sum == 0 && !c);
    controller.set_flag('c', c);
    controller.set_flag('a', ac);
    controller.set_register(Register::A, sum);
    controller.update_flags();
}

fn dadd(controller : &mut Microcontroller, b : Word, c : Byte) {
    let a = controller.get_register_pair(Register::H);
    let sum : Word = a + b + c as Word;
    let c = (a as Word + b as Word + c as Word) > 255;
    controller.set_flag('c', c);
    controller.set_register_pair(Register::H, sum);
}

// opcode ce
#[allow(dead_code)]
pub fn aci(controller : &mut Microcontroller) {
    let data = controller.fetch();
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, data, carry);
}

// opcode 8f
#[allow(dead_code)]
pub fn adc_a(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::A);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 88
#[allow(dead_code)]
pub fn adc_b(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::B);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 89
#[allow(dead_code)]
pub fn adc_c(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::C);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 8a
#[allow(dead_code)]
pub fn adc_d(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::D);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 8b
#[allow(dead_code)]
pub fn adc_e(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::E);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 8c
#[allow(dead_code)]
pub fn adc_h(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::H);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 8d
#[allow(dead_code)]
pub fn adc_l(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::L);
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, other, carry);
}

// opcode 8e
// TODO: verify if working
#[allow(dead_code)]
pub fn adc_m(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::H);
    let value_m = controller.memory[addr as usize];
    let carry = controller.register_array.status_register & 0b00000001;
    add(controller, value_m, carry);
}

// opcode 87
#[allow(dead_code)]
pub fn add_a(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::A);
    add(controller, other, 0);
}

// opcode 80
#[allow(dead_code)]
pub fn add_b(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::B);
    add(controller, other, 0);
}

// opcode 81
#[allow(dead_code)]
pub fn add_c(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::C);
    add(controller, other, 0);
}

// opcode 82
#[allow(dead_code)]
pub fn add_d(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::D);
    add(controller, other, 0);
}

// opcode 83
#[allow(dead_code)]
pub fn add_e(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::E);
    add(controller, other, 0);
}

// opcode 84
#[allow(dead_code)]
pub fn add_h(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::H);
    add(controller, other, 0);
}

// opcode 85
#[allow(dead_code)]
pub fn add_l(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::L);
    add(controller, other, 0);
}

// opcode 86
#[allow(dead_code)]
pub fn add_m(controller : &mut Microcontroller) {
    let addr : u16 = controller.get_register_pair(Register::H);
    let value_m = controller.memory[addr as usize];
    add(controller, value_m, 0);
}

// opcode c6
#[allow(dead_code)]
pub fn adi(controller : &mut Microcontroller) {
    let other = controller.fetch();
    add(controller, other, 0);
}

// opcode a7
#[allow(dead_code)]
pub fn ana_a(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::A);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a0
#[allow(dead_code)]
pub fn ana_b(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::B);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a1
#[allow(dead_code)]
pub fn ana_c(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::C);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a2
#[allow(dead_code)]
pub fn ana_d(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::D);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a3
#[allow(dead_code)]
pub fn ana_e(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::E);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a4
#[allow(dead_code)]
pub fn ana_h(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::H);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a5
#[allow(dead_code)]
pub fn ana_l(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::L);
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode a6
#[allow(dead_code)]
pub fn ana_m(controller : &mut Microcontroller) {
    let address = controller.get_register_pair(Register::H);
    let acc = controller.get_register(Register::A);
    let result = controller.memory[address as usize] & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode e6
#[allow(dead_code)]
pub fn ani(controller : &mut Microcontroller) {
    let other = controller.fetch();
    let acc = controller.get_register(Register::A);
    let result = other & acc;
    controller.set_register(Register::A, result);
    controller.set_flag('z', !result);
}

// opcode cd
#[allow(dead_code)]
pub fn call(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    let dest = controller.fetch_word();
    controller.set_program_counter(dest);
}

// opcode dc
#[allow(dead_code)]
pub fn cc(controller : &mut Microcontroller) {
    if controller.check_flag('c') {
        call(controller);
    }
}

// opcode fc
#[allow(dead_code)]
pub fn cm(controller : &mut Microcontroller) {
    if controller.check_flag('c') {
        call(controller);
    }
}

// opcode 2f
#[allow(dead_code)]
pub fn cma(controller : &mut Microcontroller) {
    controller.set_register(Register::A, !controller.get_register(Register::A));
}

// opcode 3f
#[allow(dead_code)]
pub fn cmc(controller : &mut Microcontroller) {
    controller.set_flag('c', !controller.check_flag('c'));
}

fn cmp(controller : &mut Microcontroller, a : Byte, b : Byte) {
    match a.cmp(&b) {
        Ordering::Less => {
            controller.set_flag('c', true);
            controller.set_flag('z', false);
        },
        Ordering::Equal => {
            controller.set_flag('c', false);
            controller.set_flag('z', true);
        },
        Ordering::Greater => {
            controller.set_flag('s', false);
            controller.set_flag('z', false);
        }
    }
}

// opcode bf
#[allow(dead_code)]
pub fn cmp_a(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::A);
    cmp(controller, accu, comp);
}

// opcode b8
#[allow(dead_code)]
pub fn cmp_b(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::B);
    cmp(controller, accu, comp);
}

// opcode b9
#[allow(dead_code)]
pub fn cmp_c(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::C);
    cmp(controller, accu, comp);
}

// opcode ba
#[allow(dead_code)]
pub fn cmp_d(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::D);
    cmp(controller, accu, comp);
}

// opcode bb
#[allow(dead_code)]
pub fn cmp_e(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::E);
    cmp(controller, accu, comp);
}

// opcode bc
#[allow(dead_code)]
pub fn cmp_h(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::H);
    cmp(controller, accu, comp);
}

// opcode bd
#[allow(dead_code)]
pub fn cmp_l(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.get_register(Register::L);
    cmp(controller, accu, comp);
}

// opcode be
#[allow(dead_code)]
pub fn cmp_m(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.memory[controller.get_register_pair(Register::H) as usize];
    cmp(controller, accu, comp);
}

// opcode d4
#[allow(dead_code)]
pub fn cnc(controller : &mut Microcontroller) {
    if !controller.check_flag('c') {
        call(controller);
    }
}

// opcode c4
#[allow(dead_code)]
pub fn cnz(controller : &mut Microcontroller) {
    if !controller.check_flag('z') {
        call(controller);
    }
}

// opcode f4
#[allow(dead_code)]
pub fn cp(controller : &mut Microcontroller) {
    if controller.check_flag('c') {
        call(controller);
    }
}

// opcode ec
#[allow(dead_code)]
pub fn cpe(controller : &mut Microcontroller) {
    if controller.check_flag('p') {
        call(controller);
    }
}

// opcode fe
#[allow(dead_code)]
pub fn cpi(controller : &mut Microcontroller) {
    let accu = controller.get_register(Register::A);
    let comp = controller.fetch();
    cmp(controller, accu, comp);
}

// opcode e4
#[allow(dead_code)]
pub fn cpo(controller : &mut Microcontroller) {
    if !controller.check_flag('p') {
        call(controller);
    }
}

// opcode cc
#[allow(dead_code)]
pub fn cz(controller : &mut Microcontroller) {
    if controller.check_flag('z') {
        call(controller);
    }
}

#[allow(dead_code)]
pub fn daa(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::A);
    let nibble_low = val & 0b00001111;
    if nibble_low > 9 {
        add(controller, 6, 0);
    }
    if (val & 0b11110000) > 9 {
        controller.set_flag('c', true);
    }
}

#[allow(dead_code)]
pub fn dad_b(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::B);
    let carry = controller.register_array.status_register & 0b00000001;
    dadd(controller, val, carry);
}

#[allow(dead_code)]
pub fn dad_d(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::D);
    let carry = controller.register_array.status_register & 0b00000001;
    dadd(controller, val, carry);
}

#[allow(dead_code)]
pub fn dad_h(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::H);
    let carry = controller.register_array.status_register & 0b00000001;
    dadd(controller, val, carry);
}

#[allow(dead_code)]
pub fn dad_sp(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::SP);
    let carry = controller.register_array.status_register & 0b00000001;
    dadd(controller, val, carry);
}

#[allow(dead_code)]
pub fn dcr_a(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::A, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_b(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::B);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::B, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_c(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::C);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::C, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_d(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::D);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::D, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_e(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::E);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::E, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_h(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::H);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::H, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_l(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::L);
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::L, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcr_m(controller : &mut Microcontroller) {
    let mut val = controller.memory[controller.get_register_pair(Register::H) as usize];
    if val == 0 {
        val = 0xFF;
    } else {
        val -= 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.memory[controller.get_register_pair(Register::H) as usize] = val;
    let store = controller.get_register(Register::A);
    controller.set_register(Register::A, val);
    controller.update_flags();
    controller.set_register(Register::A, store);
}

#[allow(dead_code)]
pub fn dcx_b(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::B);
    if val == 0 {
        val = 0xFFFF;
        // controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::B, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcx_d(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::D);
    if val == 0 {
        val = 0xFFFF;
        // controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::D, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcx_h(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::H);
    if val == 0 {
        val = 0xFFFF;
        // controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::H, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn dcx_sp(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::SP);
    if val == 0 {
        val = 0xFFFF;
        // controller.set_flag('c', true);
    } else {
        val -= 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::SP, val);
    controller.update_flags();
}

pub fn di(controller : &mut Microcontroller) {
    controller.disable_interrupts();
}

pub fn ei(controller : &mut Microcontroller) {
    controller.enable_interrupts();
}

#[allow(dead_code)]
pub fn hlt(controller : &mut Microcontroller) {
    controller.stop();
}

pub fn input(controller : &mut Microcontroller) {
    let addr = controller.fetch();
    let val = controller.read_io(addr);
    controller.set_register(Register::A, val);
}

#[allow(dead_code)]
pub fn inr_a(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::A, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_b(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::B);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::B, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_c(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::C);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::C, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_d(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::D);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::D, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_e(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::E);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        val += 1;
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::E, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_h(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::H);
    if val == 0xFF {
        controller.set_flag('c', true);
        val = 0;
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::H, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_l(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::L);
    if val == 0xFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register(Register::L, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inr_m(controller : &mut Microcontroller) {
    let mut val = controller.memory[controller.get_register_pair(Register::H) as usize];
    if val == 0xFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 128 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.memory[controller.get_register_pair(Register::H) as usize] = val;
    let store = controller.get_register(Register::A);
    controller.set_register(Register::A, val);
    controller.update_flags();
    controller.set_register(Register::A, store);
}

#[allow(dead_code)]
pub fn inx_b(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::B);
    if val == 0xFFFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::B, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inx_d(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::D);
    if val == 0xFFFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::D, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inx_h(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::H);
    if val == 0xFFFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::H, val);
    controller.update_flags();
}

#[allow(dead_code)]
pub fn inx_sp(controller : &mut Microcontroller) {
    let mut val = controller.get_register_pair(Register::SP);
    if val == 0xFFFF {
        val = 0;
        controller.set_flag('c', true);
    } else {
        val += 1;
    }
    if val >= 32768 {
        controller.set_flag('s', true);
    } else {
        controller.set_flag('s', false);
    }
    controller.set_flag('z', val == 0);
    controller.set_register_pair(Register::SP, val);
    controller.update_flags();
}

// opcode c3
#[allow(dead_code)]
pub fn jmp(controller : &mut Microcontroller) { 
    let next = ((controller.fetch() as u16) << 8) + (controller.fetch() as u16);
    controller.set_program_counter(next);
}

// opcode da
#[allow(dead_code)]
pub fn jc(controller : &mut Microcontroller) {
    if controller.check_flag('c') {
        jmp(controller);
    }
}

// opcode d2
#[allow(dead_code)]
pub fn jnc(controller : &mut Microcontroller) {
    if !controller.check_flag('c') {
        jmp(controller);
    }
}

pub fn jpe(controller : &mut Microcontroller) {
    if controller.check_flag('p') {
        jmp(controller);
    }
}

pub fn jpo(controller : &mut Microcontroller) {
    if !controller.check_flag('p') {
        jmp(controller);
    }
}

pub fn jp(controller : &mut Microcontroller) {
    if !controller.check_flag('s') {
        jmp(controller);
    }
}

pub fn jm(controller : &mut Microcontroller) {
    if controller.check_flag('s') {
        jmp(controller);
    }
}

// opcode ca
#[allow(dead_code)]
pub fn jz(controller : &mut Microcontroller) {
    if controller.check_flag('z') {
        jmp(controller);
    }
}

// opcode c2
#[allow(dead_code)]
pub fn jnz(controller : &mut Microcontroller) {
    if !controller.check_flag('z') {
        jmp(controller);
    }
}

#[allow(dead_code)]
pub fn lda(controller : &mut Microcontroller) {
    let address = controller.fetch_word();
    controller.register_array.register_a = controller.memory[address as usize];
}

#[allow(dead_code)]
pub fn lxi_b(controller : &mut Microcontroller) {
    let val = controller.fetch_word();
    controller.set_register_pair(Register::B, val);
}

#[allow(dead_code)]
pub fn lxi_d(controller : &mut Microcontroller) {
    let val = controller.fetch_word();
    controller.set_register_pair(Register::D, val);
}

#[allow(dead_code)]
pub fn lxi_h(controller : &mut Microcontroller) {
    let val = controller.fetch_word();
    controller.set_register_pair(Register::H, val);
}

#[allow(dead_code)]
pub fn lxi_sp(controller : &mut Microcontroller) {
    let val = controller.fetch_word();
    controller.set_register_pair(Register::SP, val);
}

// opcode 0A
#[allow(dead_code)]
pub fn ldax_b(controller : &mut Microcontroller) {
    let mut addr = controller.register_array.register_b as usize;
    addr <<= 8;
    addr |= controller.register_array.register_c as usize;
    controller.register_array.register_a = controller.memory[addr];
}

// opcode 1A
#[allow(dead_code)]
pub fn ldax_d(controller : &mut Microcontroller) {
    let mut addr = controller.register_array.register_d as usize;
    addr <<= 8;
    addr |= controller.register_array.register_e as usize;
    controller.register_array.register_a = controller.memory[addr];
}

#[allow(dead_code)]
pub fn lhld(controller : &mut Microcontroller) {
    let mut addr = controller.fetch() as usize;
    addr <<= 8;
    addr |= controller.fetch() as usize;
    controller.set_register(Register::H, controller.memory[addr as usize]);
    controller.set_register(Register::L, controller.memory[(addr + 1) as usize]);
}

fn mov(controller : &mut Microcontroller, to : Register, from : Register) {
    let value = controller.get_register(from);
    controller.set_register(to, value);
}

pub fn mov_a_a(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::A);
}

pub fn mov_a_b(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::B);
}

pub fn mov_a_c(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::C);
}

pub fn mov_a_d(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::D);
}

pub fn mov_a_e(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::E);
}

pub fn mov_a_h(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::H);
}

pub fn mov_a_l(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::L);
}

pub fn mov_a_m(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::M);
}

pub fn mov_b_a(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::A);
}

pub fn mov_b_b(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::B);
}

pub fn mov_b_c(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::C);
}

pub fn mov_b_d(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::D);
}

pub fn mov_b_e(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::E);
}

pub fn mov_b_h(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::H);
}

pub fn mov_b_l(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::L);
}

pub fn mov_b_m(controller : &mut Microcontroller) {
    mov(controller, Register::B, Register::M);
}

pub fn mov_c_a(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::A);
}

pub fn mov_c_b(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::B);
}

pub fn mov_c_c(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::C);
}

pub fn mov_c_d(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::D);
}

pub fn mov_c_e(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::E);
}

pub fn mov_c_h(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::H);
}

pub fn mov_c_l(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::L);
}

pub fn mov_c_m(controller : &mut Microcontroller) {
    mov(controller, Register::C, Register::M);
}

pub fn mov_d_a(controller : &mut Microcontroller) {
    mov(controller, Register::A, Register::A);
}

pub fn mov_d_b(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::B);
}

pub fn mov_d_c(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::C);
}

pub fn mov_d_d(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::D);
}

pub fn mov_d_e(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::E);
}

pub fn mov_d_h(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::H);
}

pub fn mov_d_l(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::L);
}

pub fn mov_d_m(controller : &mut Microcontroller) {
    mov(controller, Register::D, Register::M);
}

pub fn mov_e_a(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::A);
}

pub fn mov_e_b(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::B);
}

pub fn mov_e_c(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::C);
}

pub fn mov_e_d(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::D);
}

pub fn mov_e_e(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::E);
}

pub fn mov_e_h(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::H);
}

pub fn mov_e_l(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::L);
}

pub fn mov_e_m(controller : &mut Microcontroller) {
    mov(controller, Register::E, Register::M);
}

pub fn mov_h_a(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::A);
}

pub fn mov_h_b(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::B);
}

pub fn mov_h_c(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::C);
}

pub fn mov_h_d(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::D);
}

pub fn mov_h_e(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::E);
}

pub fn mov_h_h(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::H);
}

pub fn mov_h_l(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::L);
}

pub fn mov_h_m(controller : &mut Microcontroller) {
    mov(controller, Register::H, Register::M);
}

pub fn mov_l_a(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::A);
}

pub fn mov_l_b(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::B);
}

pub fn mov_l_c(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::C);
}

pub fn mov_l_d(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::D);
}

pub fn mov_l_e(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::E);
}

pub fn mov_l_h(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::H);
}

pub fn mov_l_l(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::L);
}

pub fn mov_l_m(controller : &mut Microcontroller) {
    mov(controller, Register::L, Register::M);
}

pub fn mov_m_a(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::A);
}

pub fn mov_m_b(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::B);
}

pub fn mov_m_c(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::C);
}

pub fn mov_m_d(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::D);
}

pub fn mov_m_e(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::E);
}

pub fn mov_m_h(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::H);
}

pub fn mov_m_l(controller : &mut Microcontroller) {
    mov(controller, Register::M, Register::L);
}

pub fn mvi_a(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::A, data);
}

pub fn mvi_b(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::B, data);
}

pub fn mvi_c(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::C, data);
}

pub fn mvi_d(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::D, data);
}

pub fn mvi_e(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::E, data);
}

pub fn mvi_h(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::H, data);
}

pub fn mvi_l(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::L, data);
}

pub fn mvi_m(controller : &mut Microcontroller) {
    let data = controller.fetch();
    controller.set_register(Register::M, data);
}

#[allow(dead_code)]
pub fn noop(_controller : &mut Microcontroller) {}

fn ora(controller : &mut Microcontroller, other : Byte) {
    let mut val = controller.get_register(Register::A);
    val |= other;
    controller.set_register(Register::A, val);
    if val == 0 {
        controller.set_flag('z', true);
    }
}

pub fn ora_a(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::A);
    ora(controller, other);
}

pub fn ora_b(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::B);
    ora(controller, other);
}

pub fn ora_c(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::C);
    ora(controller, other);
}

pub fn ora_d(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::D);
    ora(controller, other);
}

pub fn ora_e(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::E);
    ora(controller, other);
}

pub fn ora_h(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::E);
    ora(controller, other);
}

pub fn ora_l(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::L);
    ora(controller, other);
}

pub fn ora_m(controller : &mut Microcontroller) {
    let other = controller.get_register(Register::M);
    ora(controller, other);
}

pub fn ori(controller : &mut Microcontroller) {
    let other = controller.fetch();
    ora(controller, other);
}

pub fn output(controller : &mut Microcontroller) {
    let addr = controller.fetch();
    let val = controller.get_register(Register::A);
    controller.write_io(addr, val);
}

pub fn pchl(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::H);
    controller.set_program_counter(val);
}

pub fn pop_b(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = ((controller.memory[(addr + 1) as usize] as Word) << 8) 
                + controller.memory[addr as usize] as Word;
    controller.set_register_pair(Register::SP, addr + 2);
    controller.set_register_pair(Register::B, val);
}

pub fn pop_d(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = ((controller.memory[(addr + 1) as usize] as Word) << 8) 
                + controller.memory[addr as usize] as Word;
    controller.set_register_pair(Register::SP, addr + 2);
    controller.set_register_pair(Register::D, val);
}

pub fn pop_h(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = ((controller.memory[(addr + 1) as usize] as Word) << 8) 
                + controller.memory[addr as usize] as Word;
    controller.set_register_pair(Register::SP, addr + 2);
    controller.set_register_pair(Register::H, val);
}

pub fn pop_psw(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = ((controller.memory[(addr + 1) as usize] as Word) << 8) 
                + controller.memory[addr as usize] as Word;
    controller.set_register_pair(Register::SP, addr + 2);
    controller.set_register_pair(Register::PSW, val);
}

pub fn push_b(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = controller.get_register(Register::B);
    let val_low = controller.get_register(Register::C);
    controller.memory[addr as usize] = val;
    controller.memory[(addr - 1) as usize] = val_low;
    controller.set_register_pair(Register::SP, addr - 2);
}

pub fn push_d(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = controller.get_register(Register::D);
    let val_low = controller.get_register(Register::E);
    controller.memory[addr as usize] = val;
    controller.memory[(addr - 1) as usize] = val_low;
    controller.set_register_pair(Register::SP, addr - 2);
}

pub fn push_h(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = controller.get_register(Register::H);
    let val_low = controller.get_register(Register::L);
    controller.memory[addr as usize] = val;
    controller.memory[(addr - 1) as usize] = val_low;
    controller.set_register_pair(Register::SP, addr - 2);
}

pub fn push_psw(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let val = controller.get_register(Register::A);
    let val_low = controller.get_register(Register::F);
    controller.memory[addr as usize] = val;
    controller.memory[(addr - 1) as usize] = val_low;
    controller.set_register_pair(Register::SP, addr - 2);
}

pub fn ral(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    let carry = val & 0b10000000 == 0b10000000;
    let prev_carry = controller.check_flag('c');
    controller.set_flag('c', carry);
    val <<= 1;
    val |= match prev_carry {
        true => 0b00000001,
        false => 0b00000000
    };
    controller.set_register(Register::A, val);
}

pub fn rar(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    let carry = val & 0b00000001 == 0b00000001;
    let prev_carry = controller.check_flag('c');
    controller.set_flag('c', carry);
    val >>= 1;
    val |= match prev_carry {
        true => 0b10000000,
        false => 0b00000000
    };
    controller.set_register(Register::A, val);
}

pub fn rc(controller : &mut Microcontroller) {
    let carry = controller.check_flag('c');
    if carry {
        ret(controller);
    }
}

pub fn ret(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let pc = ((controller.memory[(addr + 1) as usize] as Word) << 8)
                + controller.memory[addr as usize] as Word;
    controller.set_program_counter(pc);
    controller.set_stack_pointer(addr + 2);
}

pub fn rim(_controller : &mut Microcontroller) {
    // TODO: Add implementation
}

pub fn rlc(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    let carry = val & 0b10000000 == 0b10000000;
    controller.set_flag('c', carry);
    val <<= 1;
    val |= match carry {
        true => 0b00000001,
        false => 0b00000000
    };
    controller.set_register(Register::A, val);
}

pub fn rm(controller : &mut Microcontroller) {
    let sign = controller.check_flag('s');
    if sign {
        ret(controller);
    }
}

pub fn rp(controller : &mut Microcontroller) {
    let sign = controller.check_flag('s');
    if !sign {
        ret(controller);
    }
}

pub fn rpe(controller : &mut Microcontroller) {
    let parity = controller.check_flag('p');
    if parity {
        ret(controller);
    }
}

pub fn rpo(controller : &mut Microcontroller) {
    let parity = controller.check_flag('p');
    if parity {
        ret(controller);
    }
}

pub fn rrc(controller : &mut Microcontroller) {
    let mut val = controller.get_register(Register::A);
    let carry = val & 0b00000001 == 0b00000001;
    controller.set_flag('c', carry);
    val >>= 1;
    val |= match carry {
        true => 0b10000000,
        false => 0b00000000
    };
    controller.set_register(Register::A, val);
}

pub fn rnc(controller : &mut Microcontroller) {
    let carry = controller.check_flag('c');
    if !carry {
        ret(controller);
    }
}

pub fn rz(controller : &mut Microcontroller) {
    let zero = controller.check_flag('z');
    if zero {
        ret(controller);
    }
}

pub fn rnz(controller : &mut Microcontroller) {
    let zero = controller.check_flag('z');
    if zero {
        ret(controller);
    }
}

#[allow(dead_code)]
pub fn rst_0(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(0);
}

#[allow(dead_code)]
pub fn rst_1(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(8);
}

#[allow(dead_code)]
pub fn rst_2(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(16);
}

#[allow(dead_code)]
pub fn rst_3(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(24);
}

#[allow(dead_code)]
pub fn rst_4(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(32);
}

#[allow(dead_code)]
pub fn rst_5(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(40);
}

#[allow(dead_code)]
pub fn rst_6(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(48);
}

#[allow(dead_code)]
pub fn rst_7(controller : &mut Microcontroller) {
    let stp = controller.get_stack_pointer();
    controller.set_stack_pointer(stp + 2);
    controller.memory[(stp - 1) as usize] = (controller.get_program_counter() << 8) as u8;
    controller.memory[stp as usize] = (controller.get_program_counter() & 0b1111111100000000) as u8;
    controller.set_program_counter(56);
}

// TODO: Recheck and fix this possibly broken code ( considering the fact that its 20 mins past midnight at the time of writing this code and I need sleep :( )
fn sub(controller : &mut Microcontroller, b : Byte, c : Byte) {
    let a = controller.get_register(Register::A);
    let sum : Byte = a - b - c;
    let ac = (((a & 0b00001111) - c) - (b & 0b00001111)) > 0b00001001;
    let c = (a as i32 - b as i32 - c as i32) < 0;
    controller.set_flag('c', c);
    controller.set_flag('a', ac);
    controller.set_flag('z', sum == 0);
    controller.set_register(Register::A, sum);
    controller.update_flags();
}

pub fn sub_a(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::A);
    sub(controller, val, 0);
}

pub fn sub_b(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::B);
    sub(controller, val, 0);
}

pub fn sub_c(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::C);
    sub(controller, val, 0);
}

pub fn sub_d(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::D);
    sub(controller, val, 0);
}

pub fn sub_e(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::E);
    sub(controller, val, 0);
}

pub fn sub_h(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::H);
    sub(controller, val, 0);
}

pub fn sub_l(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::L);
    sub(controller, val, 0);
}

pub fn sub_m(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::M);
    sub(controller, val, 0);
}

pub fn sbb_a(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::A);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_b(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::B);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_c(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::C);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_d(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::D);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_e(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::E);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_h(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::H);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_l(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::L);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbb_m(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::M);
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn sbi(controller : &mut Microcontroller) {
    let val = controller.fetch();
    let carry = controller.register_array.status_register & 0b00000001;
    sub(controller, val, carry);
}

pub fn shld(controller : &mut Microcontroller) {
    let addr = controller.fetch_word();
    let l = controller.get_register(Register::L);
    let h = controller.get_register(Register::H);
    if addr == 0xFFFF {
        controller.memory[0xFFFF as usize] = l;
    } else {
        controller.memory[addr as usize] = l;
        controller.memory[(addr + 1) as usize] = h;
    }
}

pub fn sim(_controller : &mut Microcontroller) {
    // TODO: Implement
}

pub fn sui(controller : &mut Microcontroller) {
    let val = controller.fetch();
    sub(controller, val, 0);
}

pub fn sphl(controller : &mut Microcontroller) {
    let val = controller.get_register_pair(Register::H);
    controller.set_register_pair(Register::SP, val);
}

// opcode 32
#[allow(dead_code)]
pub fn sta(controller : &mut Microcontroller) {
    let mut addr = controller.fetch() as usize;
    addr |= (controller.fetch() as usize) << 8;
    controller.memory[addr] = controller.register_array.register_a;
}

pub fn stax_b(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::B);
    let val = controller.get_register(Register::A);
    controller.memory[addr as usize] = val;
}

pub fn stax_d(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::D);
    let val = controller.get_register(Register::A);
    controller.memory[addr as usize] = val;
}

pub fn stc(controller : &mut Microcontroller) {
    controller.set_flag('c', true);
}

pub fn xchg(controller : &mut Microcontroller) {
    let h = controller.get_register_pair(Register::H);
    let d = controller.get_register_pair(Register::D);
    controller.set_register_pair(Register::D, h);
    controller.set_register_pair(Register::H, d);
}

fn xra(controller : &mut Microcontroller, other : Byte) {
    let mut val = controller.get_register(Register::A);
    val ^= other;
    controller.set_register(Register::A, val);
    controller.set_flag('z', val == 0);
}

pub fn xra_a(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::A);
    xra(controller, val);
}

pub fn xra_b(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::B);
    xra(controller, val);
}

pub fn xra_c(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::C);
    xra(controller, val);
}

pub fn xra_d(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::D);
    xra(controller, val);
}

pub fn xra_e(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::E);
    xra(controller, val);
}

pub fn xra_h(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::H);
    xra(controller, val);
}

pub fn xra_l(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::L);
    xra(controller, val);
}

pub fn xra_m(controller : &mut Microcontroller) {
    let val = controller.get_register(Register::M);
    xra(controller, val);
}

pub fn xri(controller : &mut Microcontroller) {
    let val = controller.fetch();
    xra(controller, val);
}

pub fn xthl(controller : &mut Microcontroller) {
    let addr = controller.get_register_pair(Register::SP);
    let limit = 0xFFFF;
    let l = controller.memory[addr as usize];
    let h = match addr.cmp(&limit) {
        Ordering::Greater | Ordering::Equal => controller.memory[0 as usize],
        Ordering::Less => controller.memory[(addr + 1) as usize]
    };
    controller.set_register(Register::H, h);
    controller.set_register(Register::L, l);
}
