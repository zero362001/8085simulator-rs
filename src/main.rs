mod microcontroller;
mod operations;
mod assembler;
mod gui;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use assembler::Assembler;


fn main() {
    // let path = Path::new("code.asm");
    // let out_path = Path::new("assembled.obj");
    // let display = path.display();
    // let display_out = out_path.display();
    // let mut file = match File::open(&path) {
    //     Err(why) => panic!("Failed to open {} : {}!", display, why),
    //     Ok(file) => file
    // };
    // let mut contents = String::new();
    // match file.read_to_string(&mut contents) {
    //     Err(why) => panic!("Failed to read contents of {} : {}", display, why),
    //     Ok(_) => println!("Read source code from file successfully")
    // };
    // let mut assembler = Assembler::new(&contents);
    // let assembled = assembler.assemble();
    // let mut target_file = match File::create(out_path) {
    //     Err(why) => panic!("Failed to open {} : {}!", display_out, why),
    //     Ok(file) => file
    // };
    // match target_file.write(&assembled) {
    //     Err(why) => panic!("Failed to write contents to file : {}", why),
    //     Ok(count) => println!("Wrote {} bytes!", count)
    // };
    let mut app = gui::create_gtk_app();
    gui::setup_app(&mut app);
}
